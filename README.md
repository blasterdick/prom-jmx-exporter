JMX exporter for Prometheus with http-server from [here](https://github.com/prometheus/jmx_exporter).

Starting this container makes JMX stats for Prometheus scrape available via http on `http://127.0.0.1:9124/metrics`.

Example of `config.yml`:

```
---
hostPort: 127.0.0.1:9123
username:
password:
ssl: false
lowercaseOutputName: false
attrNameSnakeCase: true

rules:
- pattern: 'java.lang<type=Memory><HeapMemoryUsage>(.+): .*'
  name: java_lang_Memory_HeapMemoryUsage_$1
- pattern: 'java.lang<type=Memory><NonHeapMemoryUsage>(.+): .*'
  name: java_lang_Memory_NonHeapMemoryUsage_$1
- pattern: 'java.lang<type=OperatingSystem><.*>OpenFileDescriptorCount: .*'
  name: java_lang_OperatingSystem_OpenFileDescriptorCount
- pattern: 'java.lang<type=OperatingSystem><.*>ProcessCpuLoad: .*'
  name: java_lang_OperatingSystem_ProcessCpuLoad
- pattern: 'java.lang<type=Threading><(.*)>ThreadCount: .*'
  name: java_lang_Threading_ThreadCount

```

This config expects java app with JMX enabled on `9123` tcp port, running on the same machine (kubernetes pod, etc.)

To start java with JMX enabled, use:

```
java -cp app.jar -Dcom.sun.management.jmxremote -Djava.net.preferIPv4Stack=true \
-Dcom.sun.management.jmxremote.port=9123 \ -Dcom.sun.management.jmxremote.rmi.port=9122 \ 
-Dcom.sun.management.jmxremote.authenticate=false \
-Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.local.only=false \
-Djava.rmi.server.hostname=127.0.0.1 com.elharo.math.algebra.fields

```