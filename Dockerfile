FROM java:8-alpine

MAINTAINER Dmitriy Ansimov "d.ansimov@owox.com"

ENV JMX_VERSION="0.9"

WORKDIR /app

ADD https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_httpserver/$JMX_VERSION/jmx_prometheus_httpserver-$JMX_VERSION-jar-with-dependencies.jar /app/jmx_prometheus.jar
ADD container-files/config.yml /config.yml

EXPOSE 9124

CMD [ "java", "-jar", "/app/jmx_prometheus.jar", "9124", "/config.yml" ]
